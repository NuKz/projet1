﻿using System;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmAccueil : Form
    {
        public frmAccueil()
        {
            InitializeComponent();
        }

        private void btnAjoutHotel_Click(object sender, EventArgs e)
        {
            Form f = new frmAjoutHotel();
            f.Show();
        }

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnVoirHotel_Click(object sender, EventArgs e)
        {
            Form f = new frmVoirHotels();
            f.Show();
        }

        private void btnAjoutChambre_Click(object sender, EventArgs e)
        {
            Form f = new frmAjoutChambre();
            f.Show();
        }

        private void btnVoirChambre_Click(object sender, EventArgs e)
        {
            Form f = new frmAjoutChambre();
            f.Show();
        }
    }
}
