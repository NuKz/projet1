﻿using System;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmVoirHotels : Form
    {
        public frmVoirHotels()
        {
            InitializeComponent();
        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmVoirHotels_Load(object sender, EventArgs e)
        {
            lstHotels.DataSource = Persistance.getLesHotels();

        }
    }
}
