﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class VoirChambre : Form
    {
        public VoirChambre()
        {
            InitializeComponent();
        }

        private void VoirChambre_Load(object sender, EventArgs e)
        {
            lstChambre.DataSource = Persistance.getLesChambres();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
